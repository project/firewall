## INTRODUCTION

The Firewall module allows you to control inbound access based on rules with host, paths, methods, and parameter combination. It's a kind of self controlled "[Web application firewall](https://en.wikipedia.org/wiki/Web_application_firewall)".

The first idea behind this module is restrict access to a full featured Drupal via different hosts for example in this way:
- "**admin.example.com**" can be added to bypass list and should be secured by server controlled authentication or e.g. shield module if not available.
- "**editor.example.com**" can also be protected by server. But there you can also add firewall rules to deny access to "/admin" paths and redirect zo admin.example.com.
- "**public.example.com**" can get a firewall rule to allow all GET requests. But you can limit PUSH requests to single paths like /form/contact and allow only a list of parameters that are allowed to send.
With the possibility to bypass via client IP you can allow access to special API paths and block them in "public.example.com".</li>

## REQUIREMENTS

DESCRIBE_MODULE_DEPENDENCIES_HERE

## INSTALLATION

Install as you would normally install a contributed Drupal module.
See: https://www.drupal.org/node/895232 for further information.

## CONFIGURATION
There is no UI. The configuration is only possible via settings.php to keep it very lightweight because of the [Middleware situation](https://www.drupal.org/docs/8/api/middleware-api/overview). Keep sure that all host you would give access are not protected via core trusted_hosts setting.

### Example config:
```php
$settings['firewall']['enabled'] = TRUE;
$settings['firewall']['bypass']['hosts'] = ['admin.example.com'];
$settings['firewall']['bypass']['client_ips'] = ['127.0.0.1'];
$settings['firewall']['rules']['editor.example.com'] = [
  [
    'path' => '/admin',
    'redirect' => TRUE,
    'redirect_base' => "https://admin.example.com",
    // When path is not set the request path will be used.
    'redirect_path' => '',
    // 'path_part' is used insetad of using '*' in path.
    'path_part' => TRUE,
    // With exit there is no check of the following path rules.
    'exit' => TRUE,
    'mode' => 'deny',
    // In deny mode parameter key matches the deny logic.
    'methods' => ['GET' => ['*'], 'POST' => ['*']],
  ],
  [
    'path' => '*',
    'exit' => FALSE,
    'mode' => 'allow',
    // In allow mode the parameter needs to be in the list to match 'allow'
    'methods' => ['GET' => ['*'], 'POST' => ['*']],
  ],
];
$settings['firewall']['rules']['public.example.com'] = [
  [
    'path' => '/special/api',
    'exit' => TRUE,
    'mode' => 'deny',
    'methods' => ['GET' => ['*'], 'POST' => ['*']],
  ],
  [
    'path' => '*',
    'exit' => FALSE,
    'mode' => 'allow',
    // Allow GET with not parameters sent (empty query).
    'methods' => ['GET' => []]
  ],
  [
    'path' => '/form/contact',
    'mode' => 'allow',
    'methods' => [
      'GET' => [],
      // POST parameters used by default contact form config of webform module.
      'POST' => ['name', 'email', 'subject', 'message', 'op', 'form_id', 'form_build_id'],
     ],
  ],
];
```
## MAINTAINERS

Current maintainers for Drupal 10:

- Carsten Logemann (C-Logemann) - https://www.drupal.org/u/C-Logemann

