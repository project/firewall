<?php

declare(strict_types=1);

namespace Drupal\firewall;

use Drupal\Core\Site\Settings;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\HttpKernelInterface;

/**
 * Provides a HTTP middleware to implement Firewall rules.
 */
class FirewallHttpKernelMiddleware implements HttpKernelInterface {

  /**
   * The decorated kernel.
   *
   * @var \Symfony\Component\HttpKernel\HttpKernelInterface
   */
  protected $httpKernel;

  /**
   * Constructs a FirewallMiddleware object.
   *
   * @param \Symfony\Component\HttpKernel\HttpKernelInterface $http_kernel
   *   The decorated kernel.
   */
  public function __construct(HttpKernelInterface $http_kernel) {
    $this->httpKernel = $http_kernel;
  }

  /**
   * {@inheritdoc}
   */
  public function handle(Request $request, $type = self::MAIN_REQUEST, $catch = TRUE): Response {
    $settings = Settings::get('firewall', []);
    $enabled = $settings['enabled'] ?? FALSE;
    if ($enabled == FALSE) {
      return $this->httpKernel->handle($request, $type, $catch);
    }

    $redirect_url = '';
    $deny = TRUE;
    $deny_msg = $settings['deny_msg'] ?? 'Access denied by application firewall.';

    $requested_host = $request->getHost();
    $bypass_hosts = $settings['bypass']['hosts'] ?? [];
    if ($bypass_hosts != []) {
      if (\in_array($requested_host, $bypass_hosts)) {
        return $this->httpKernel->handle($request, $type, $catch);
      }
    }
    $bypass_client_ips = $settings['bypass']['client_ips'] ?? [];
    if ($bypass_client_ips != []) {
      $client_ip = $request->getClientIp();
      if (\in_array($client_ip, $bypass_client_ips)) {
        return $this->httpKernel->handle($request, $type, $catch);
      }
    }

    $requested_path = $request->getPathInfo();
    $active_method = $request->getMethod();

    if ($active_method == 'POST') {
      $params_sent = $request->request->all() ?? [];
    }
    else {
      $params_sent = $request->query->all() ?? [];
    }
    $params_sent_keys = \array_keys($params_sent) ?? [];

    $rules = $settings['rules'] ?? [];
    foreach ($rules as $this_host => $host_rules) {
      $host_match = FALSE;
      if ($this_host == '*') {
        $host_match = TRUE;
      }
      elseif ($requested_host == $this_host) {
        $host_match = TRUE;
      }
      if ($host_match) {
        foreach ($host_rules as $rule) {
          $this_path_match = FALSE;
          $this_path = $rule['path'];
          $this_path_part = $rule['path_part'] ?? FALSE;
          $this_mode = $rule['mode'] ?? 'deny';
          $this_exit = $rule['exit'] ?? TRUE;
          $this_redirect = $rule['redirect'] ?? FALSE;
          $this_redirect_base = $rule['redirect_base'] ?? '';
          $this_redirect_path = $rule['redirect_path'] ?? $requested_path;
          if ($this_path == '*') {
            $this_path_match = TRUE;
          }
          else {
            if ($this_path_part) {
              if (\strncmp($requested_path, $this_path, strlen($this_path)) === 0) {
                $this_path_match = TRUE;
              }
            }
            elseif ($this_path == $requested_path) {
              $this_path_match = TRUE;
            }
          }

          if ($this_path_match) {
            $this_method_param_match = FALSE;
            $this_methods = $rule['methods'] ?? [];
            $this_keys = $this_methods[$active_method] ?? [];
            if ($this_keys == []) {
              if ($this_mode == 'deny') {
                // No need to check Parameter.
                $this_method_param_match = TRUE;
              }
              else {
                // Allow GET with empty query parameter.
                if ($params_sent == []) {
                  $this_method_param_match = TRUE;
                }
              }
            }
            else {
              if ($this_mode == 'allow' && $params_sent == []) {
                $this_method_param_match = TRUE;
              }
              else {
                if ($this_keys[0] == '*') {
                  $this_method_param_match = TRUE;
                }
                else {
                  $this_key_match = FALSE;
                  foreach ($params_sent_keys as $params_sent_key) {
                    if ($this_mode == 'deny') {
                      if (\in_array($params_sent_key, $this_keys)) {
                        // True if at least one key is in (deny) list.
                        $this_key_match = TRUE;
                        continue;
                      }
                    }
                    else {
                      if (!\in_array($params_sent_key, $this_keys)) {
                        // True if one key is not in (allow) list.
                        $this_key_match = TRUE;
                        break;
                      }
                    }
                  }

                  if ($this_key_match == TRUE && $this_mode == 'deny') {
                    $this_method_param_match = TRUE;
                  }
                  if ($this_key_match == FALSE && $this_mode == 'allow') {
                    $this_method_param_match = TRUE;
                  }
                }
              }
            }
            if ($this_method_param_match) {
              if ($this_redirect) {
                $redirect_url = $this_redirect_base . $this_redirect_path;
              }
              if ($this_mode == 'allow') {
                $deny = FALSE;
              }
              else {
                $deny = TRUE;
              }
              if ($this_exit) {
                break;
              }
            }
          }
        }
      }
    }

    if ($deny) {
      if ($redirect_url != '') {
        $redirect_status = $settings['redirect_status'] ?? 302;
        $redirect_headers = $settings['redirect_headers'] ?? [];
        return new RedirectResponse($redirect_url, $redirect_status, $redirect_headers);
      }
      throw new BadRequestHttpException($deny_msg);
    }
    return $this->httpKernel->handle($request, $type, $catch);
  }

}
